void main() {
  var application = new Applications();

  application.Printinfo();
}

class Applications {
  var apps = [
    {
      'name': 'fnb',
      'year': 2012,
      'Catagory': 'Best Overall App',
      'Developer': 'fnb'
    },
    {
      'name': 'SnapScan',
      'year': 2013,
      'Catagory': 'Best HTML 5 app',
      'Developer': 'Kobus Ehlers'
    },
    {
      'name': 'Live Inspect',
      'year': 2014,
      'Catagory': 'Best Android app',
      'Developer': 'Lightsone'
    },
    {
      'name': 'Wumdrop',
      'year': 2015,
      'Catagory': 'Best Enterprise',
      'Developer': 'Benjamin Classen & Muneeb Samuels',
    },
    {
      'name': 'Domestly',
      'year': 2016,
      'Catagory': 'Best Consumer Solution',
      'Developer': 'Thatoyoana Marumo & Berno Potgieters',
    },
     {
      'name': 'Shyft',
      'year': 2017,
      'Catagory': 'Best Financial Solution',
      'Developer': 'Brett Patrontasch',
    },
     {
      'name': 'Khula ecosystem',
      'year': 2018,
      'Catagory': 'Best Agriculture Solution',
      'Developer': 'Karidas Tshintsholo',
    },
    {
      'name': 'Naked Insurance',
      'year': 2019,
      'Catagory': 'Best Agriculture Solution',
      'Developer': 'Sumarie Greybe & Ernest North &Alex Thomson',
    },
    {
      'name': 'EasyEquities',
      'year': 2020,
      'Catagory': 'Best Consumer Solution',
      'Developer': 'Charle Savage',
    },
     {
      'name': 'Ambani Africa',
      'year': 2021,
      'Catagory': 'Best Consumer Solution',
      'Developer': 'Mukundi Lambani',
    },
    
  ];

  void Printinfo() {
    print('MTN App of the year past winners from 2012 to 2021');

    for (var app in apps) {
      print('$app'.toUpperCase());
    }
  }
}
